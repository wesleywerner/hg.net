# What is hg.Net?
A .Net class library that provides you with functionality to integrate Mercurial into your applications.

# What is Mercurial?
A source control management system, more info at the Mercurial home page.

# What is hg.Net written in?
VB.Net for framework version 2. I have not tested this in Mono yet.

# More details please
The wrapper features a couple of output parsers, useful for processing log and status commands into objects, with an inheritable parser base from which you can construct your own parsers.

The wrapper also allows you to execute any custom command, with an arbitrary number of arguments. It has built-in support to ensure cross-thread safety, allowing you to use the asynchronous events in your applications while it remains responsive (No mucking around with Invoke and Delegates!)

# How do I get the library?
There is a compiled DLL available in the Download section, or grab the Source code and compile it yourself!

# How do I use the library?
The source code solution comes with a test project demonstrating the library. It is also available as a separate download.