<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BSDLicenseForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.txtLicense = New System.Windows.Forms.TextBox
		Me.btnIAgree = New System.Windows.Forms.Button
		Me.SuspendLayout()
		'
		'txtLicense
		'
		Me.txtLicense.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
					Or System.Windows.Forms.AnchorStyles.Left) _
					Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtLicense.BackColor = System.Drawing.SystemColors.Window
		Me.txtLicense.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLicense.Location = New System.Drawing.Point(12, 12)
		Me.txtLicense.Multiline = True
		Me.txtLicense.Name = "txtLicense"
		Me.txtLicense.ReadOnly = True
		Me.txtLicense.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtLicense.Size = New System.Drawing.Size(501, 484)
		Me.txtLicense.TabIndex = 0
		'
		'btnIAgree
		'
		Me.btnIAgree.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnIAgree.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.btnIAgree.Location = New System.Drawing.Point(438, 502)
		Me.btnIAgree.Name = "btnIAgree"
		Me.btnIAgree.Size = New System.Drawing.Size(75, 23)
		Me.btnIAgree.TabIndex = 1
		Me.btnIAgree.Text = "I Agree"
		Me.btnIAgree.UseVisualStyleBackColor = True
		'
		'BSDLicenseForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(525, 537)
		Me.ControlBox = False
		Me.Controls.Add(Me.btnIAgree)
		Me.Controls.Add(Me.txtLicense)
		Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "BSDLicenseForm"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "License Agreement"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
    Friend WithEvents txtLicense As System.Windows.Forms.TextBox
    Friend WithEvents btnIAgree As System.Windows.Forms.Button

End Class
