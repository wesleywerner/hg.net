Imports System.Windows.Forms

Public Class BSDLicenseForm

	Private Const year As String = "2009"

	Private Const lic As String = "Copyright (c) {0} {1} All rights reserved. " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "All rights reserved. " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.  " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.  " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "3. Neither the name of {1} nor the names of its contributors may promote products derived from this software without specific prior written permission.  " & ControlChars.CrLf & _
	  " " & ControlChars.CrLf & _
	  "THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ""AS IS"" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."


	''' <summary>
	''' form load
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub LicenseForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Try

			txtLicense.Text = String.Format(lic, _
			year, _
			My.Application.Info.CompanyName, _
			My.Application.Info.ProductName)

			txtLicense.SelectionStart = 0
			txtLicense.SelectionLength = 0

		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1, 0)
		End Try

	End Sub

End Class
