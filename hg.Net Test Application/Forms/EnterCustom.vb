Imports System.Windows.Forms

Public Class EnterCustom


	Public Property customCommand() As String
		Get
			Return _customCommand
		End Get
		Set(ByVal value As String)
			_customCommand = value
		End Set
	End Property
	Private _customCommand As String



	Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
		customCommand = TextBox1.Text
		Me.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.Close()
	End Sub

	Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
		Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.Close()
	End Sub

End Class
