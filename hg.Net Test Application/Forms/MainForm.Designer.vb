<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.LogGrid = New System.Windows.Forms.DataGridView
		Me.txtLog = New System.Windows.Forms.TextBox
		Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
		Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
		Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
		Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
		Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
		Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
		Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
		Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
		Me.openTool = New System.Windows.Forms.ToolStripButton
		Me.statusTool = New System.Windows.Forms.ToolStripButton
		Me.logTool = New System.Windows.Forms.ToolStripButton
		Me.cloneTool = New System.Windows.Forms.ToolStripDropDownButton
		Me.clonePath = New System.Windows.Forms.ToolStripMenuItem
		Me.cloneURL = New System.Windows.Forms.ToolStripMenuItem
		Me.pushTool = New System.Windows.Forms.ToolStripButton
		Me.pullTool = New System.Windows.Forms.ToolStripButton
		Me.addTool = New System.Windows.Forms.ToolStripButton
		Me.addRemoveTool = New System.Windows.Forms.ToolStripButton
		Me.commitTool = New System.Windows.Forms.ToolStripButton
		Me.customTool = New System.Windows.Forms.ToolStripButton
		Me.licTool = New System.Windows.Forms.ToolStripButton
		Me.revertTool = New System.Windows.Forms.ToolStripButton
		CType(Me.LogGrid, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.ToolStrip1.SuspendLayout()
		Me.SplitContainer1.Panel1.SuspendLayout()
		Me.SplitContainer1.Panel2.SuspendLayout()
		Me.SplitContainer1.SuspendLayout()
		Me.SuspendLayout()
		'
		'LogGrid
		'
		Me.LogGrid.AllowUserToAddRows = False
		Me.LogGrid.AllowUserToDeleteRows = False
		Me.LogGrid.BackgroundColor = System.Drawing.SystemColors.Control
		Me.LogGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.LogGrid.Dock = System.Windows.Forms.DockStyle.Fill
		Me.LogGrid.Location = New System.Drawing.Point(0, 0)
		Me.LogGrid.Name = "LogGrid"
		Me.LogGrid.ReadOnly = True
		Me.LogGrid.Size = New System.Drawing.Size(750, 103)
		Me.LogGrid.TabIndex = 0
		'
		'txtLog
		'
		Me.txtLog.Dock = System.Windows.Forms.DockStyle.Fill
		Me.txtLog.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLog.Location = New System.Drawing.Point(0, 0)
		Me.txtLog.Multiline = True
		Me.txtLog.Name = "txtLog"
		Me.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.txtLog.Size = New System.Drawing.Size(750, 219)
		Me.txtLog.TabIndex = 1
		'
		'ToolStrip1
		'
		Me.ToolStrip1.AutoSize = False
		Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.openTool, Me.ToolStripSeparator3, Me.statusTool, Me.logTool, Me.ToolStripSeparator5, Me.cloneTool, Me.ToolStripSeparator2, Me.pushTool, Me.pullTool, Me.ToolStripSeparator1, Me.addTool, Me.addRemoveTool, Me.ToolStripSeparator6, Me.revertTool, Me.commitTool, Me.ToolStripSeparator4, Me.customTool, Me.licTool})
		Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
		Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
		Me.ToolStrip1.Name = "ToolStrip1"
		Me.ToolStrip1.Size = New System.Drawing.Size(750, 64)
		Me.ToolStrip1.TabIndex = 2
		Me.ToolStrip1.Text = "ToolStrip1"
		'
		'ToolStripSeparator3
		'
		Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
		Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 23)
		'
		'ToolStripSeparator5
		'
		Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
		Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 23)
		'
		'ToolStripSeparator2
		'
		Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
		Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 23)
		'
		'ToolStripSeparator1
		'
		Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
		Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 23)
		'
		'ToolStripSeparator6
		'
		Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
		Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 23)
		'
		'ToolStripSeparator4
		'
		Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
		Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 23)
		'
		'SplitContainer1
		'
		Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.SplitContainer1.Location = New System.Drawing.Point(0, 64)
		Me.SplitContainer1.Name = "SplitContainer1"
		Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
		'
		'SplitContainer1.Panel1
		'
		Me.SplitContainer1.Panel1.Controls.Add(Me.txtLog)
		'
		'SplitContainer1.Panel2
		'
		Me.SplitContainer1.Panel2.Controls.Add(Me.LogGrid)
		Me.SplitContainer1.Size = New System.Drawing.Size(750, 326)
		Me.SplitContainer1.SplitterDistance = 219
		Me.SplitContainer1.TabIndex = 3
		'
		'openTool
		'
		Me.openTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.folder_open2
		Me.openTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.openTool.Name = "openTool"
		Me.openTool.Size = New System.Drawing.Size(109, 20)
		Me.openTool.Text = "open local repo"
		'
		'statusTool
		'
		Me.statusTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.info
		Me.statusTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.statusTool.Name = "statusTool"
		Me.statusTool.Size = New System.Drawing.Size(59, 20)
		Me.statusTool.Text = "Status"
		'
		'logTool
		'
		Me.logTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.text
		Me.logTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.logTool.Name = "logTool"
		Me.logTool.Size = New System.Drawing.Size(83, 20)
		Me.logTool.Text = "get file log"
		'
		'cloneTool
		'
		Me.cloneTool.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.clonePath, Me.cloneURL})
		Me.cloneTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.copyall
		Me.cloneTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.cloneTool.Name = "cloneTool"
		Me.cloneTool.Size = New System.Drawing.Size(92, 20)
		Me.cloneTool.Text = "clone repo"
		'
		'clonePath
		'
		Me.clonePath.Name = "clonePath"
		Me.clonePath.Size = New System.Drawing.Size(98, 22)
		Me.clonePath.Text = "Path"
		'
		'cloneURL
		'
		Me.cloneURL.Name = "cloneURL"
		Me.cloneURL.Size = New System.Drawing.Size(98, 22)
		Me.cloneURL.Text = "URL"
		'
		'pushTool
		'
		Me.pushTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.go_up
		Me.pushTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.pushTool.Name = "pushTool"
		Me.pushTool.Size = New System.Drawing.Size(53, 20)
		Me.pushTool.Text = "push"
		'
		'pullTool
		'
		Me.pullTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.go_down
		Me.pullTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.pullTool.Name = "pullTool"
		Me.pullTool.Size = New System.Drawing.Size(47, 20)
		Me.pullTool.Text = "pull"
		'
		'addTool
		'
		Me.addTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.add
		Me.addTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.addTool.Name = "addTool"
		Me.addTool.Size = New System.Drawing.Size(96, 20)
		Me.addTool.Text = "add new files"
		'
		'addRemoveTool
		'
		Me.addRemoveTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.add
		Me.addRemoveTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.addRemoveTool.Name = "addRemoveTool"
		Me.addRemoveTool.Size = New System.Drawing.Size(95, 20)
		Me.addRemoveTool.Text = "add+remove"
		'
		'commitTool
		'
		Me.commitTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.upload
		Me.commitTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.commitTool.Name = "commitTool"
		Me.commitTool.Size = New System.Drawing.Size(69, 20)
		Me.commitTool.Text = "commit"
		'
		'customTool
		'
		Me.customTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.play
		Me.customTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.customTool.Name = "customTool"
		Me.customTool.Size = New System.Drawing.Size(125, 20)
		Me.customTool.Text = "custom command"
		'
		'licTool
		'
		Me.licTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.open_source
		Me.licTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.licTool.Name = "licTool"
		Me.licTool.Size = New System.Drawing.Size(66, 20)
		Me.licTool.Text = "License"
		'
		'revertTool
		'
		Me.revertTool.Image = Global.hgNet_Test_Application.My.Resources.Resources.undo
		Me.revertTool.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.revertTool.Name = "revertTool"
		Me.revertTool.Size = New System.Drawing.Size(60, 20)
		Me.revertTool.Text = "Revert"
		'
		'MainForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(750, 390)
		Me.Controls.Add(Me.SplitContainer1)
		Me.Controls.Add(Me.ToolStrip1)
		Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Name = "MainForm"
		Me.Text = "hg.Net Test"
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		CType(Me.LogGrid, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ToolStrip1.ResumeLayout(False)
		Me.ToolStrip1.PerformLayout()
		Me.SplitContainer1.Panel1.ResumeLayout(False)
		Me.SplitContainer1.Panel1.PerformLayout()
		Me.SplitContainer1.Panel2.ResumeLayout(False)
		Me.SplitContainer1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents LogGrid As System.Windows.Forms.DataGridView
	Friend WithEvents txtLog As System.Windows.Forms.TextBox
	Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
	Friend WithEvents logTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents addTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents pushTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents pullTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents statusTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents addRemoveTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
	Friend WithEvents commitTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents customTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents cloneTool As System.Windows.Forms.ToolStripDropDownButton
	Friend WithEvents clonePath As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents cloneURL As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents openTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents licTool As System.Windows.Forms.ToolStripButton
	Friend WithEvents revertTool As System.Windows.Forms.ToolStripButton
End Class
