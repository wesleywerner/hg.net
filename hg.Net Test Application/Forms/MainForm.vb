Public Class MainForm

	' stores the location of our local repo
	Private local_repo As String = Nothing

	' the hg.Net wrapper exposes some useful events, like asynchronous output capturing!
	Private WithEvents wrap As hgNet.Wrapper


	''' <summary>
	''' entry point
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try

			' prompt the user to select a lcoal repo
			openTool.PerformClick()

			' stop here if they did not choose a repo
			If IsNothing(local_repo) Then
				Me.Close()
				Exit Sub
			End If

			' create a new wrapper instance
			' the constructor gets the Mercurial version, and will throw an exception
			' if the command did not execute
			wrap = New hgNet.Wrapper(local_repo)

			' set the control we want to synchronize our threads with.
			' this will allow us to make cross-thread safely.
			' if we did not set this, and we do something like txtLog.txt = e.Output from within
			' the wrap.OutputReceived() Event, we will get a cross-thread operation exception
			wrap.SynchronizeControl = txtLog

			' show the hg version info
			txtLog.AppendText(wrap.Version & ControlChars.CrLf)

			' show repo into
			wrap.Custom("status", "-v")

		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' prompt the user to select a local repo
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub openTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openTool.Click

		' prompt the user for the local repo location
		Dim dlg As New FolderBrowserDialog
		dlg.Description = "SELECT YOUR LOCAL REPO (create and select an empty folder if you want to clone a repo)"
		dlg.SelectedPath = My.Settings.LastUsedFolder

		If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then

			' remember the folder for next time
			local_repo = dlg.SelectedPath
			My.Settings.LastUsedFolder = dlg.SelectedPath

			' set the new wrapper repo
			' (wrap is empty on first load, we set it at startup)
			If Not IsNothing(wrap) Then wrap.Local_Repository = local_repo

		End If

	End Sub


	''' <summary>
	''' Asynchronous output received
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub wrap_OutputReceived(ByVal sender As Object, ByVal e As hgNet.Internals.CommandEventArgs) Handles wrap.OutputReceived
		Try

			If (wrap.ExitCode = 0) Then
				' exit code 0 = all okay
				txtLog.AppendText(e.Output & ControlChars.CrLf)
			Else
				' other exit codes are issues
				txtLog.AppendText(String.Format("exit code {0}: {1}{2}", wrap.ExitCode, e.Output, ControlChars.CrLf))
			End If

			My.Application.DoEvents()

		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Demonstrates how to use the log results as a data source
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub logTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logTool.Click
		Try
			Dim results As List(Of hgNet.Results.FileLogResult)
			results = wrap.Log("")
			LogGrid.DataMember = ""
			LogGrid.DataSource = results
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Add new files to the local repo
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub addTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addTool.Click
		Try
			wrap.Add(local_repo)
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Push changesets
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub pushTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pushTool.Click
		Try
			wrap.Push()
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Pull changeset
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub pullTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pullTool.Click
		Try
			wrap.Pull()
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Get the status log as managed objects
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub statusTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles statusTool.Click

		Try

			' note: we can call wrap.Custom("status") too
			' but this method will parse the results
			' into manageble objects for us

			Dim results As List(Of hgNet.Results.StatusResult)
			results = wrap.Status()

			If results.Count = 0 Then
				txtLog.AppendText("Nothing to report" & ControlChars.CrLf)
			Else
				For Each item As hgNet.Results.StatusResult In results
					txtLog.AppendText(String.Format("{0} {1}{2}", item.Code, item.Filename, ControlChars.CrLf))
				Next
			End If

		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Commit the working folder
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub commitTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles commitTool.Click
		Try
			wrap.Commit()
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Execute a custom command
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub customTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles customTool.Click
		Try
			If My.Forms.EnterCustom.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
				Dim cmds() As String = My.Forms.EnterCustom.customCommand.Split(" "c)
				If cmds.Length > 0 Then
					wrap.Custom(cmds)
				End If
			End If
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Clone a repo from a path
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub clonePath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clonePath.Click
		Try
			Dim dlg As New FolderBrowserDialog
			dlg.Description = "SELECT THE LOCATION OF THE REPO TO CLONE"
			dlg.SelectedPath = My.Settings.LastRepo
			If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
				My.Settings.LastRepo = dlg.SelectedPath
				wrap.Clone(dlg.SelectedPath, local_repo)
			End If
		Catch ex As Exception
			MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
		End Try
	End Sub


	''' <summary>
	''' Clone a repo from an URL
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub cloneURL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cloneURL.Click
		If My.Forms.URLPrompt.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
			My.Settings.LastRepo = My.Forms.URLPrompt.URL
			wrap.Clone(My.Forms.URLPrompt.URL, local_repo)
		End If
	End Sub


	''' <summary>
	''' Show the license form
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub licTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles licTool.Click
		My.Forms.BSDLicenseForm.ShowDialog()
	End Sub


	''' <summary>
	''' Revert all local files
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	''' <remarks></remarks>
	Private Sub revertTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles revertTool.Click

		If MessageBox.Show("Revert all your local files, with the --no-backup option, to the contents they had in the parent of the working directory?", _
		 Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

			' we revert with extra options:
			' --no-backup = do not save backup copies of files	
			wrap.Revert(New String() {"--no-backup"}, ".")

		End If

	End Sub

End Class
