Imports System.Windows.Forms

Public Class URLPrompt

	Public ReadOnly Property URL() As String
		Get
			Return txtURL.Text
		End Get
	End Property

	Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
		Me.DialogResult = System.Windows.Forms.DialogResult.OK
		Me.Close()
	End Sub

	Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
		Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.Close()
	End Sub

	Private Sub txtURL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtURL.TextChanged
		OK_Button.Enabled = txtURL.Text.Length > 0
	End Sub

	Private Sub URLPrompt_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		txtURL.Text = My.Settings.LastRepo
	End Sub
End Class
