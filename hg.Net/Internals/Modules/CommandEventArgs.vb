Namespace Internals
	''' <summary>
	''' A class to contain Command event data
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Public Class CommandEventArgs
		Inherits System.EventArgs

		''' <summary>
		''' The command output text
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Output() As String
			Get
				Return _Output
			End Get
			Set(ByVal value As String)
				_Output = value
			End Set
		End Property
		Private _Output As String

		Sub New(ByVal e As System.EventArgs)

		End Sub

		Sub New(ByVal output As String)
			Me.Output = output
		End Sub

	End Class
End Namespace
