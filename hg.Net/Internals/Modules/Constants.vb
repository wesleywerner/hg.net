Namespace Constants
	''' <summary>
	''' Constants used by the wrapper
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Module Constants

		''' <summary>
		''' The end-of-result string defines where the result block ends, and the next one begins
		''' </summary>
		''' <remarks></remarks>
		Public Const EOR As String = ChrW(10) & ChrW(10) & "changeset:"

		''' <summary>
		''' The end-of-line string defines the line delimiter
		''' </summary>
		''' <remarks></remarks>
		Public Const EOL As String = ChrW(10)

	End Module
End Namespace
