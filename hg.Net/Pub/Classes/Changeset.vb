Namespace Results
	''' <summary>
	''' A class to parse and contain changeset details
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Public Class Changeset


		''' <summary>
		''' The changeset Revision number
		''' Only valid within this repository
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Rev() As Integer
			Get
				Return _Rev
			End Get
			Set(ByVal value As Integer)
				_Rev = value
			End Set
		End Property
		Private _Rev As Integer


		''' <summary>
		''' The hexadecimal changeset ID
		''' This is the same in all repositories that contain this changeset
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property ChangesetID() As String
			Get
				Return _ChangesetID
			End Get
			Set(ByVal value As String)
				_ChangesetID = value
			End Set
		End Property
		Private _ChangesetID As String


		''' <summary>
		''' Create a new empty changeset object
		''' </summary>
		''' <remarks></remarks>
		Sub New()

		End Sub


		''' <summary>
		''' Create a new changeset object with the given values
		''' </summary>
		''' <param name="rev"></param>
		''' <param name="changeset_id"></param>
		''' <remarks></remarks>
		Sub New(ByVal rev As Integer, ByVal changeset_id As String)
			Me.Rev = rev
			Me.ChangesetID = changeset_id
		End Sub


		''' <summary>
		''' Parse the given changeset string
		''' </summary>
		''' <param name="text"></param>
		''' <returns></returns>
		''' <remarks></remarks>
		Shared Function Parse(ByVal text As String) As Changeset
			Dim idx As Integer
			idx = text.IndexOf(":"c)
			If (idx > -1) Then
				Dim item As New Changeset
				item.Rev = CInt(text.Substring(0, idx))
				item.ChangesetID = text.Substring(idx + 1)
				Return item
			Else
				Throw New Exception("The changeset string is in an incorrect format. It should be 'rev:changeset_id'")
			End If
		End Function


		''' <summary>
		''' Returns the formatted changeset revision and ID string
		''' </summary>
		''' <returns></returns>
		''' <remarks></remarks>
		Overrides Function ToString() As String
			Return String.Concat(Me.Rev, ":", Me.ChangesetID)
		End Function

	End Class
End Namespace
