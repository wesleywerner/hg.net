Namespace Results
	''' <summary>
	''' A class to contain log details
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Public Class FileLogResult
		Inherits Internals.ResultBase


#Region " properties "

		''' <summary>
		''' The changeset
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property [Changeset]() As Changeset
			Get
				Return _changeset
			End Get
		End Property
		Private _changeset As Changeset

		''' <summary>
		''' The date
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property [Date]() As String
			Get
				Return _date
			End Get
		End Property
		Private _date As String

		''' <summary>
		''' The user
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property User() As String
			Get
				Return _user
			End Get
		End Property
		Private _user As String

		''' <summary>
		''' The change description
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property Description() As String
			Get
				Return _desc
			End Get
		End Property
		Private _desc As String

		''' <summary>
		''' The files in this change
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property Files() As String
			Get
				Return _files
			End Get
		End Property
		Private _files As String

#End Region


		''' <summary>
		''' Create a new file log object
		''' </summary>
		''' <param name="result_text"></param>
		''' <remarks></remarks>
		Sub New(ByVal result_text As String)
			MyBase.New(result_text)
			Store_values()
		End Sub


		''' <summary>
		''' Store the results into variables
		''' </summary>
		''' <remarks></remarks>
		Private Sub Store_values()

			' changeset
			_changeset = Changeset.Parse(Me("changeset").Value)

			' date
			_date = Me("date").Value

			' description
			_desc = Me("description").Value

			' user
			_user = Me("user").Value

			' files
			_files = Me("files").Value

		End Sub


		''' <summary>
		''' Parse the log results
		''' </summary>
		''' <remarks></remarks>
		Public Overrides Sub Parse_Lines()
			Me.Add(Parse_Value("changeset"))
			Me.Add(Parse_Value("date"))
			Me.Add(Parse_Value("user"))
			Me.Add(Parse_Value("description", hgNet.Constants.EOR))
			Me.Add(Parse_Value("files"))
		End Sub

	End Class
End Namespace
