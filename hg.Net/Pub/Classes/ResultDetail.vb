Namespace Results
	''' <summary>
	''' A class to store result details
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Public Class ResultDetail

		''' <summary>
		''' The detail key
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Key() As String
			Get
				Return _Key
			End Get
			Set(ByVal value As String)
				_Key = value
			End Set
		End Property
		Private _Key As String

		''' <summary>
		''' The detail value
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Value() As String
			Get
				Return _Value
			End Get
			Set(ByVal value As String)
				_Value = value
			End Set
		End Property
		Private _Value As String

		''' <summary>
		''' Create a new detail
		''' </summary>
		''' <remarks></remarks>
		Sub New()

		End Sub

		''' <summary>
		''' Create a new detail with the given key and value
		''' </summary>
		''' <param name="key"></param>
		''' <param name="value"></param>
		''' <remarks></remarks>
		Sub New(ByVal key As String, ByVal value As String)
			Me.Key = key
			Me.Value = value
		End Sub

	End Class
End Namespace
