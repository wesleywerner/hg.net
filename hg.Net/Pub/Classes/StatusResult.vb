Namespace Results
	''' <summary>
	''' A class that contains status results
	''' (hg.Net by Wesley Werner)
	''' </summary>
	''' <remarks></remarks>
	Public Class StatusResult
		Inherits Internals.ResultBase


#Region " properties "

		''' <summary>
		''' The status code
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Code() As String
			Get
				Return _Code
			End Get
			Set(ByVal value As String)
				_Code = value
			End Set
		End Property
		Private _Code As String


		''' <summary>
		''' The filename
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public Property Filename() As String
			Get
				Return _Filename
			End Get
			Set(ByVal value As String)
				_Filename = value
			End Set
		End Property
		Private _Filename As String


		''' <summary>
		''' Is this file modified (code M)
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property IsModified() As Boolean
			Get
				Return Code = "M"
			End Get
		End Property


		''' <summary>
		''' Is this file unknown (code ?)
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property IsUnknown() As Boolean
			Get
				Return Code = "?"
			End Get
		End Property


		''' <summary>
		''' Is this file added (code A)
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Public ReadOnly Property IsAdded() As Boolean
			Get
				Return Code = "A"
			End Get
		End Property


#End Region


		''' <summary>
		''' Create a new status result object
		''' </summary>
		''' <param name="result_text">The result text to parse</param>
		''' <remarks></remarks>
		Sub New(ByVal result_text As String)
			MyBase.New(result_text)

			' parse the results into variables
			Dim idx As Integer
			idx = result_text.IndexOf(" "c)

			If (idx = 1) Then
				Me.Code = result_text.Substring(0, 1)
				Me.Filename = result_text.Substring(2)
				Me.Add(New ResultDetail("code", Me.Code))
				Me.Add(New ResultDetail("filename", Me.Filename))
			End If

		End Sub


		''' <summary>
		''' Parse the log results
		''' </summary>
		''' <remarks></remarks>
		Public Overrides Sub Parse_Lines()
			' custom parsing done in the constructor
			' because we dont use the Base Parse_Lines() method
		End Sub


	End Class
End Namespace
